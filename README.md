# Arima Stock Price

Predicting stock prices using an ARIMA model.

Click [Here](https://gitlab.com/urkem/arima_stock_price/-/blob/master/ARIMA.ipynb) to open jupyter notebook.

# Results

![Prediction Image](https://i.imgur.com/KIXDM85.png)
